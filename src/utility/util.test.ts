import { Util } from './util';

describe("Testing invalidScoresMethod", () => {
  let scorecap = 10;
  it("check a valid score", () => {
    let score1 = 1;
    let score2 = 2;
    expect(Util.invalidScores(score1, score2, scorecap)).toBe(false);
  });

  describe("Scores over the scorecap returns invalid", () => {
    it("first score too large returns invalid", () => {
      let score1 = 11;
      let score2 = 5;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });

    it("second score too large returns invalid", () => {
      let score1 = 5;
      let score2 = 11;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });
  })

  describe("Negative scores are invalid", () => {
    it("first score is negative", () => {
      let score1 = -1;
      let score2 = 5;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    })

    it("second score is negative", () => {
      let score1 = 2;
      let score2 = -1;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    })
  });

  describe("Non integral scores returns invalid", () => {
    it("first score non integral", () => {
      let score1 = 1.5;
      let score2 = 3;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });

    it("second score non integral", () => {
      let score1 = 5;
      let score2 = 3.2;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });
  });

  describe("Null scores are invalid", () => {
    it("first score is NaN", () => {
      let score1 = NaN;
      let score2 = 3;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });

    it("second score is NaN", () => {
      let score1 = 5;
      let score2 = NaN;
      expect(Util.invalidScores(score1, score2, scorecap)).toBe(true);
    });
  })
})