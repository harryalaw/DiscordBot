import { Board } from './board';

describe("Testing board methods", () => {
  describe("randomiseFanAngle", () => {
    let board = new Board(["", ""]);
    it("Sets fan and dial angle to the same value", () => {
      board.randomiseFanAngle()
      expect(board.dialAngle).toEqual(board.fanAngle);
    });
  });

  describe("moveDial", () => {
    let board = new Board(["", ""]);
    it("Negative amount increases angle", () => {
      let increment = -10;
      while (board.dialAngle > 70) {
        board.randomiseFanAngle();
      }
      let prevAngle = board.dialAngle;
      board.moveDial(increment);
      expect(board.dialAngle).toEqual(prevAngle - increment);
    });

    it("Positive amount decreases angle", () => {
      let increment = 10;
      while (board.dialAngle < -70) {
        board.randomiseFanAngle();
      }
      let prevAngle = board.dialAngle;
      board.moveDial(increment);
      expect(board.dialAngle).toEqual(prevAngle - increment);
    });

    it("Dial stop at 80", () => {
      board.moveDial(-1000);
      expect(board.dialAngle).toEqual(80);
    });

    it("Dial stops at -80", () => {
      board.moveDial(1000);
      expect(board.dialAngle).toEqual(-80);
    });
  });
});

// needed to resolve issue with imports related to git
// https://stackoverflow.com/questions/67178109/jest-throwing-reference-error-about-an-import-inside-a-node-modules-dependency
afterAll(done => {
  setTimeout(done);
});
